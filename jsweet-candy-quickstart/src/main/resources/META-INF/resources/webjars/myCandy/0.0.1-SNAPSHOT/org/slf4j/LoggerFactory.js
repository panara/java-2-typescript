import { Logger } from './Logger';
export class LoggerFactory {
    static getLogger(clz) {
        return new Logger();
    }
}
LoggerFactory["__class"] = "org.slf4j.LoggerFactory";
