/* Generated from Java with JSweet 1.2.0-SNAPSHOT - http://www.jsweet.org */
var myCandy;
(function (myCandy) {
    /**
     * An example of a library programmed with JSweet. The produced Maven artifact
     * can be used by a regular Java program or by another JSweet program.
     *
     * Also, it can be used from TypeScript and JavaScript directly (see the bundles
     * generated in the 'dist' directory of this project).
     */
    var API = (function () {
        function API() {
        }
        /**
         * Tells if the given number is a prime number or not.
         *
         * <p>
         * From http://www.mkyong.com/java/how-to-determine-a-prime-number-in-java/
         * by Oscar Sanchez.
         *
         * @param n
         * the number to be tested
         * @return true if n is a prime number
         */
        API.isPrime = function (n) {
            if (n % 2 === 0)
                return false;
            for (var i = 3; i * i <= n; i += 2) {
                if (n % i === 0)
                    return false;
            }
            return true;
        };
        return API;
    }());
    myCandy.API = API;
    API["__class"] = "myCandy.API";
})(myCandy || (myCandy = {}));
/* Generated from Java with JSweet 1.2.0-SNAPSHOT - http://www.jsweet.org */
var org;
(function (org) {
    var slf4j;
    (function (slf4j) {
        var Logger = (function () {
            function Logger() {
            }
            Logger.prototype.info = function (msg) {
                console.info(msg);
            };
            return Logger;
        }());
        slf4j.Logger = Logger;
        Logger["__class"] = "org.slf4j.Logger";
    })(slf4j = org.slf4j || (org.slf4j = {}));
})(org || (org = {}));
/* Generated from Java with JSweet 1.2.0-SNAPSHOT - http://www.jsweet.org */
var org;
(function (org) {
    var slf4j;
    (function (slf4j) {
        var LoggerFactory = (function () {
            function LoggerFactory() {
            }
            LoggerFactory.getLogger = function (clz) {
                return new org.slf4j.Logger();
            };
            return LoggerFactory;
        }());
        slf4j.LoggerFactory = LoggerFactory;
        LoggerFactory["__class"] = "org.slf4j.LoggerFactory";
    })(slf4j = org.slf4j || (org.slf4j = {}));
})(org || (org = {}));
