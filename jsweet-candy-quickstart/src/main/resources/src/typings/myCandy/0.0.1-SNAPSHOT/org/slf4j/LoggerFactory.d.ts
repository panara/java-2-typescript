import { Logger } from './Logger';
export declare class LoggerFactory {
    static getLogger(clz: any): Logger;
}
