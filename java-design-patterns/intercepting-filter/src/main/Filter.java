/**
 * Filter interface
 * @author joshzambales
 *
 */
public interface Filter{
	public String execute(String[] request);
}
