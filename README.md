# This project evaluates existing Java to TypeScript Converters

## Feature Comparison Table:

| Name                | Interfaces | Source Code | JAX-RS annotations | Open Source | Comments             | URL                                                    |
|---------------------|------------|-------------|--------------------|-------------|----------------------|--------------------------------------------------------|
| ts-java             | x          |             |                    |      x      | 					   | https://github.com/RedSeal-co/ts-java                  |
| java2typescript     | x          |             |         x          |      x      |                      | https://github.com/raphaeljolivet/java2typescript      |
| typescript-generator| x          |             |         x          |      x      |                      | https://github.com/vojtechhabarta/typescript-generator |
| jsweet              | x          |      x      |                    |      x      | maven/gradle support | http://www.jsweet.org                                  |
| GWT              	  | x          |      x      |                    |      x      | unreadable code      | https://github.com/gwtproject/gwt, http://www.mccarroll.net/snippets/j2js/ |


## Applying jsweet to existing java project
* For external dependencies like slf4j own candies have to be provided (see /jsweet-candy-quickstart)
* Several sub-projects of /java-design-patterns were successfully transpiled and executed in NodeJS


## Issues faced while applying jsweet to existing java project:
* For java.lang.* classes  the following candy has to be used:  https://github.com/j4ts/j4ts
* Transpiler opiton <module>commonjs</module> doesn't work when there are candies in dependencies  (see http://www.jsweet.org/getting-started/#Transpiler_options)
* Candies have to be built with <bundle>true</bundle>

